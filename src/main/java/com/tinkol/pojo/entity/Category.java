package com.tinkol.pojo.entity;


import lombok.Data;

import java.sql.Timestamp;

@Data
public class Category {

	private Integer cateId;
	private String cateName;
	private Integer cateSort;
	private Timestamp createtime;
	private Timestamp updatetime;
	private String cateChannel;
	private Integer cateParentid;

}
