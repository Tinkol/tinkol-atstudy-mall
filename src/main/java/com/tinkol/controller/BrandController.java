package com.tinkol.controller;

import com.tinkol.pojo.entity.Brand;
import com.tinkol.service.BrandService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("brand")
@Api(value = "BrandController ")
public class BrandController {
    @Autowired
    private BrandService brandService;


    public List<Brand> selectList(){
        List<Brand> brandList = brandService.selectList();
        return brandList;
    }

}
