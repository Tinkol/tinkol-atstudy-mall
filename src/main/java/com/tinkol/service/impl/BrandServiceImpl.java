package com.tinkol.service.impl;

import com.tinkol.mapper.BrandMapper;
import com.tinkol.pojo.entity.Brand;
import com.tinkol.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    private BrandMapper brandMapper;
    @Override
    public List<Brand> selectList() {

       List<Brand>  brandList= brandMapper.selectList();
        return brandList;
    }
}
