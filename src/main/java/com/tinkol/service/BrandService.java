package com.tinkol.service;

import com.tinkol.pojo.entity.Brand;

import java.util.List;

public interface BrandService {
    List<Brand> selectList();
}
