package com.tinkol.mapper;

import com.tinkol.pojo.entity.Brand;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BrandMapper {
    List<Brand> selectList();
}
